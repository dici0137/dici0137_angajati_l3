package AngajatiApp.repository;
import AngajatiApp.model.Employee;
import org.junit.*;
import static AngajatiApp.controller.DidacticFunction.*;
import static junit.framework.TestCase.assertEquals;

public class EmployeeMockTest {

    EmployeeRepositoryInterface employees = new EmployeeMock();

    private static Employee e1, e2, e3, e4, e5, e6;
    private static Employee e7, e8, e9, e10, e11, e12;
    private static Employee e13, e14, e15, e16, e17, e18;
    private static Employee e19, e20, e21, e22, e23, e24;
    private static Employee e25, e26, e27, e28, e29, e30, e31, e32;

    @Before
    public void setUp() throws Exception {
        e1 = new Employee("AAAA","BBAA","2999999334432",ASISTENT,222.2);
        e2 = new Employee("A","BB","2999999334432",ASISTENT,222.2);
        e3 = new Employee("AA","B","2999999334432",ASISTENT,222.2);
        e4 = new Employee("AA","BB","29919999334432",ASISTENT,222.2);
        e5 = new Employee("AA","BB","2999999334432",ASISTENT,222.2);
        e6 = new Employee("AA","BB","2999A999334432",ASISTENT,222.2);
        e7 = new Employee("AA","BB","2999999334432",null,222.2);
        e8 = new Employee("A","BB","2999999334432",ASISTENT,0.0);
        e9 = new Employee("AAAA","BBBB","2999999334432",ASISTENT,222.2);
        e10 = new Employee("AAAAAAAAAA","BBBB","2999999334432",ASISTENT,222.2);
        e11 = new Employee("A AAA","BBBB","2999999334432",ASISTENT,222.2);
        e12 = new Employee("AA2","BB","2999999334432",ASISTENT,222.2);
        e13 = new Employee("AA?","BB","2999999334432",ASISTENT,222.2);
        e14 = new Employee("AA","B","2999999334432",ASISTENT,222.2);
        e15 = new Employee("AA","BB","2999999334432",ASISTENT,222.2);
        e16 = new Employee("AA","BBBBBBBBBB","2999999334432",ASISTENT,222.2);
        e17 = new Employee("AA","B B","2999999334432",ASISTENT,222.2);
        e18 = new Employee("AA","BB2","2999999334432",ASISTENT,222.2);
        e19 = new Employee("AA","BB?","2999999334432",ASISTENT,222.2);
        e20 = new Employee("AA","BB","2",ASISTENT,222.2);
        e21 = new Employee("AA","BB","2999999334432",ASISTENT,222.2);
        e22 = new Employee("AA","BB","22999999334432",ASISTENT,222.2);
        e23 = new Employee("AA","BB","29A99999334432",ASISTENT,222.2);
        e24 = new Employee("AA","BB","299 999334432",ASISTENT,222.2);
        e25 = new Employee("AA","BB","2999?99334432",ASISTENT,222.2);
        e26 = new Employee("AA","BB","2999999334432",ASISTENT,222.2);
        e27 = new Employee("AA","BB","2999999334432",null,222.2);
        e28 = new Employee("AA","BB","2999999334432",null,222.2);
        e29 = new Employee("AA","BB","2999999334432",ASISTENT,-222.2);
        e30 = new Employee("AA","BB","2999999334432",ASISTENT,0.0);
        e31 = new Employee("AA","BB","2999999334432",ASISTENT,222.2);
        e32 = new Employee("AA","BB","2999999334432",ASISTENT,null);
    }

    @After
    public void tearDown() throws Exception {
        e1 = e2 = e3 = e4 = e5 = e6 = e7 = e8 = e9 = e11 = null;
        e12 = e13 = e14 = e15 = e16 = e17 = e18 = e19 = e20 = e21 = null;
        e22 = e23 = e24 = e25 = e26 = e27 = e28 = e29 = e30 = e31 = e32 = null;
    }

    @Test
    public void modifyEmployeeFunction() {
        assertEquals(ASISTENT,e32.getFunction());
    }

    @Test
    public void modifyEmployeeFunction1() {
        assertEquals(ASISTENT,e32.getFunction());
    }
}